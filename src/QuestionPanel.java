import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class QuestionPanel extends JPanel {

	public JLabel questionLabel;
	public JLabel noteLabel;
	public JRadioButton btnChoix1;
	public JRadioButton btnChoix2;
	public JRadioButton btnChoix3;
	public JRadioButton btnChoix4;
	public JRadioButton btnChoix5;
	public JButton btnPrecedent;
	public JButton btnSuivant;
	public JRadioButton[] btnRadioButtons;
	
	//Creation du compteur
	public Compteur compteurPanel = new Compteur(60);

	
	public ButtonGroup buttonGroup;
	
	public static int numeroQuestion = 0 ;
	
	
	Joueur joueur ;

	public QuestionPanel() {
		

		// Jpanels pour l'affichage des questions
		JPanel centerPanelQuestion = new JPanel(new GridLayout(5, 0));

		// Pour aligner de maniere vertical les cases a cocher
		JPanel basPanelQuestion = new JPanel(new FlowLayout(FlowLayout.LEFT));

		noteLabel = new JLabel("Note: ");
		JTextField noteField = new JTextField("0");
		questionLabel = new JLabel();
		btnChoix1 = new JRadioButton();
		btnChoix2 = new JRadioButton();
		btnChoix3 = new JRadioButton();
		btnChoix4 = new JRadioButton();
		btnChoix5 = new JRadioButton();

		btnPrecedent = new JButton("Precedent");
		btnSuivant = new JButton("Suivant");

		// Au desactive le bouton precedent au debut du jeu
		btnPrecedent.setEnabled(false);

		btnRadioButtons = new JRadioButton[] { btnChoix1, 
				btnChoix2, btnChoix3, btnChoix4, btnChoix5 };

		// Assignation des polices et bordure aux composants
		noteLabel.setFont(Police.appliquerCourrierNew());
		questionLabel.setFont(Police.appliquerCourrierNewBold());
		noteField.setFont(Police.appliquerCourrierNew());
		btnPrecedent.setFont(Police.appliquerCourrierNew());
		btnSuivant.setFont(Police.appliquerCourrierNew());

		noteLabel.setBorder(BorderFactory.createEmptyBorder(0, 250, 0, 0));
		noteField.setPreferredSize(new Dimension(100, 30));

		btnChoix1.setFont(Police.appliquerCourrierNew());
		btnChoix2.setFont(Police.appliquerCourrierNew());
		btnChoix3.setFont(Police.appliquerCourrierNew());
		btnChoix4.setFont(Police.appliquerCourrierNew());
		btnChoix5.setFont(Police.appliquerCourrierNew());

		// Assignation des Listener
		btnChoix1.addActionListener(new EtatBoutonRadioListener());
		btnChoix2.addActionListener(new EtatBoutonRadioListener());
		btnChoix3.addActionListener(new EtatBoutonRadioListener());
		btnChoix4.addActionListener(new EtatBoutonRadioListener());
		btnChoix5.addActionListener(new EtatBoutonRadioListener());

		// Assignation des commandes d'action losrqu'on clique sur un bouton Radio
		btnChoix1.setActionCommand("0");
		btnChoix2.setActionCommand("1");
		btnChoix3.setActionCommand("2");
		btnChoix4.setActionCommand("3");
		btnChoix5.setActionCommand("4");
		
		buttonGroup = new ButtonGroup();
		buttonGroup.add(btnChoix1);
		buttonGroup.add(btnChoix2);
		buttonGroup.add(btnChoix3);
		buttonGroup.add(btnChoix4);
		buttonGroup.add(btnChoix5);
		
		// On charge tous les boutons dans un tableau
		JRadioButton[] btnRadioButtons = { btnChoix1, btnChoix2, btnChoix3, btnChoix4, btnChoix5 };

		
		//Les composants du centre
		centerPanelQuestion.add(btnChoix1);
		centerPanelQuestion.add(btnChoix2);
		centerPanelQuestion.add(btnChoix3);
		centerPanelQuestion.add(btnChoix4);
		centerPanelQuestion.add(btnChoix5);
		
		//Les composants du bas
		basPanelQuestion.add(btnPrecedent);
		basPanelQuestion.add(btnSuivant);
		
		//Assignaton du layout principal (3 lignes 0 colonne)
		this.setLayout(new GridLayout(4, 0));
		
		
		//Panel pour contenir le compteur
		JPanel panel = new JPanel();
		panel.setBackground(Color.white);
		panel.setBounds(650, 18, 180, 76);
		panel.add(compteurPanel);
		this.add(panel);
		
		
		this.add(questionLabel);
		this.add(centerPanelQuestion);
		this.add(basPanelQuestion);

		


	}
	
	class EtatBoutonRadioListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			// Parcourir les enumerations de Button
			for (Enumeration<AbstractButton> enumerationButtons = buttonGroup.getElements(); enumerationButtons
					.hasMoreElements();) {

				AbstractButton button = enumerationButtons.nextElement();

				if (button.isSelected()) {
					// On recupere l'index du BoutonRadio
					joueur.indexChoix[numeroQuestion] = 
							Integer.valueOf(button.getActionCommand().toString());
					break;
				}

			}
	
		}
		
	}
	
	
	public void genererQuestionSuivante(Question[] questions) {

		// On recupere la question suivante
		Question question = questions[numeroQuestion];
		// On assigne cette question au label affichant la question
		questionLabel.setText("Question N˚" + (numeroQuestion + 1) + "/" + questions.length + ": " + question.libelle);

		// On assigne chaque boutton radio à une proposition de reponse
		for (int i = 0; i < question.reponses.length; i++) {
			String reponse = question.reponses[i];

			btnRadioButtons[i].setText(reponse);
			
		}
		//Si le joueur avait sélectionné une reponse celle ci est restaurée si il revient a cette question
		btnRadioButtons[joueur.indexChoix[numeroQuestion]].setSelected(true);
		btnRadioButtons[joueur.indexChoix[numeroQuestion]].doClick();;


	}

	
	

	
}
