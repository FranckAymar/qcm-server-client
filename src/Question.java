import java.io.Serializable;

public class Question implements Serializable {
	
	public String libelle ;
	public String[] reponses ;
	public int indexReponseCorrect ;
	
	public Question(String libelle,  String[] reponses, int indexReponseCorrect) {
	
		this.libelle = libelle;
		this.reponses = reponses ;
		this.indexReponseCorrect = indexReponseCorrect;
		
	}
}
