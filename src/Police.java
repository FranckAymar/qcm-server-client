import java.awt.Font;

public class Police {

	public static Font appliquerCourrierNew() {
		return new Font("Courier New", Font.PLAIN, 20) ;
	}
	
	public static Font appliquerLucidaSans() {
		return new Font("LucidaSans", Font.PLAIN, 20) ;
	}
	
	public static Font appliquerCourrierNewBold() {
		return new Font("Courier New", Font.BOLD, 20) ;
	}
}
