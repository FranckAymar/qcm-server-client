import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class IdentificationPanel extends JPanel {

	public JButton btnSlideSuivant;
	public JTextField champNom;
	Image imgEsatic ;

	public IdentificationPanel() {
		// TODO Auto-generated constructor stub
		
		this.setLayout(new GridLayout(3, 0));
		//Creation des panels Centre, bas pour l'organisation de la vue
		JPanel centerIdentificationPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel basIdentificationPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		JLabel bienvenuLabel = new JLabel(
				"<html>Bienvenue au concours <br> Saisissez votre Nom et Pr�noms puis cliquez sur suivant"
						+ "<br> </html>");
		JLabel labelChampNom = new JLabel("Nom et Prenom :");
		champNom = new JTextField();
		btnSlideSuivant = new JButton("Suivant");
		btnSlideSuivant.setEnabled(false);
		JButton btnSlidePrecedent = new JButton("Precedent");
		btnSlidePrecedent.setEnabled(false);
		//Taille du champ nom
		champNom.setPreferredSize(new Dimension(150, 30));
		
		//Assignation des polices au composants
		bienvenuLabel.setFont(Police.appliquerCourrierNew());
		champNom.setFont(Police.appliquerCourrierNew());
		labelChampNom.setFont(Police.appliquerCourrierNew());
		btnSlideSuivant.setFont(Police.appliquerCourrierNew());
		btnSlidePrecedent.setFont(Police.appliquerCourrierNew());

		centerIdentificationPanel.add(labelChampNom);
		centerIdentificationPanel.add(champNom);
		basIdentificationPanel.add(btnSlidePrecedent);
		basIdentificationPanel.add(btnSlideSuivant);

		this.add(bienvenuLabel);
		this.add(centerIdentificationPanel);
		this.add(basIdentificationPanel);
		
		

	}
	
	
	
}
