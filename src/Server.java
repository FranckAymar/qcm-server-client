import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public static void main(String[] args) {
		
		
		String[] reponse1 = {"Abidjan", "YamoussouKro", "Bingerville", "Korogho", "Je ne sais pas"} ;
		Question question1 = new Question("Quelle est la capitale de la Cote d'Ivoire", reponse1, 0) ;
		
		
		String[] reponse2 = {"La monarchie", "La dictature", "La d�mocratie", "Le depostisme", "Je ne sais pas"} ;
		Question question2 = new Question("Quel est le r�gime politique de la C�te d'Ivoire ?", reponse2, 2) ;

		
		String[] reponse3 = {"Am�rique", "Europe", "Asie", "Afique", "Je ne connais pas"} ;
		Question question3 = new Question("O� la C�te d'Ivoire est-elle situ�e ?", reponse3, 3) ;

		String[] reponse4 = {"Cotien", "Ivoirien", "Francais", "Gaulois", "Je ne sais pas"} ;
		Question question4 = new Question("Comment appelle t-on les habitants de la Cote d'Ivoire ?", reponse4, 1) ;
		
		String[] reponse5 = {"2009", "2003", "1994", "1995", "Je ne connais pas"} ;
		Question question5 = new Question("En quelle ann�e Java a �t�?", reponse5, 2) ;

		String[] reponse6 = {"multit�che", "un programme ex�cutable", " monno-utilisateur", " mono-tache", "Je ne sais pas"} ;
		Question question6 = new Question("Unix est un syst�me : ", reponse6, 0) ;

		String[] reponse7 = {"une instance d�un programme ex�cutable", "un programme ex�cutable", "Un contexte processeur", "1899", "Je ne connais pas"} ;
		Question question7 = new Question("Un processus est : ", reponse7, 1) ;
		
		String[] reponse8 = {"Google", "Unix", "Microsoft", "Yahoo", "Je ne connais pas"} ;
		Question question8 = new Question("Quelle entreprise finance le syst�me d�exploitation open source mobile Andro�d ?", reponse8, 0) ;

		String[] reponse9 = {"HP-UX", "Solaris", "RedHat", "SGI", "Je ne connais pas"} ;
		Question question9 = new Question("Parmi ces 4 syst�mes Unix, un est Linux, lequel ?", reponse9, 1) ;

		String[] reponse10 = {"H�ritage", "Encapsulation", "Polymorphisme", "Compilation", "Je ne sais pas"} ;
		Question question10 = new Question("Lequel des �l�ments suivants n�est pas un concept POO en Java??", reponse10, 3) ;

		String[] reponse11 = {"[0, 255]", "[0, 256]", "[0, 222]", "[-128, 127]", "Je ne sais pas"} ;
		Question question11 = new Question("Quelle est la plage de valeurs autoris�e \n pour une variable d�clar�e avec le type byte?", reponse11, 3) ;
		
		String[] reponse12 = {"Bit", "Byte", "Octet", "Binaire", "Je ne sais pas"} ;
		Question question12 = new Question("Quelle est l'unit� d'information fondamentale en informatique ?", reponse12, 0) ;
		
		
		String[] reponse13 = {"HTML", "HTTP", "Pascal", "R", "Je ne sais pas"} ;
		Question question13 = new Question("Quel est le langage informatique le plus courant utilis� pour �crire les pages web ??", reponse13, 0) ;

		
		String[] reponse14 = {" Un processeur", "Une carte video", "Un disque dur", "Une carte m�re", "Je ne connais pas"} ;
		Question question14 = new Question("Qu�est-ce qu�un CPU ?", reponse14, 0) ;

		String[] reponse15 = {"Go", "GHz", "MIPS", "Km/h", "Je ne sais pas"} ;
		Question question15 = new Question("En quelle unit� mesure-t-on la fr�quence (nombre de cycles par seconde) d�un microprocesseur ??", reponse15, 1) ;
		
		String[] reponse16 = {"Maill��", "Bus", "Etoile", "Anneau", "Je ne connais pas"} ;
		Question question16 = new Question("Internet utilise une topologie ?", reponse16, 0) ;

		String[] reponse17 = {"Glisser/d�poser", "Copier/coller", "Couper/coller", "suprimer/creer", "Je ne sais pas"} ;
		Question question17 = new Question("Que signifie l�action Drag and Drop ? ", reponse17, 0) ;

		String[] reponse18 = {"Cable", "Routeur", "ensemble d'ordinateurs", " r�seau de r�seaux", "Je ne connais pas"} ;
		Question question18 = new Question("Internet repr�sente un : ", reponse18, 3) ;
		
		String[] reponse19 = {"File Transmission Protocol", "File Transfer Protocol", "Fiber twisted pairs", "Faible Temps Partag�", "Je ne connais pas"} ;
		Question question19 = new Question("Que veut dire FTP ?", reponse19, 1) ;

		String[] reponse20 = {"CPU", "L�unit� centrale", "Moniteur", "Souris", "Je ne connais pas"} ;
		Question question20 = new Question("Comment appelle-t-on l��cran de l�ordinateur ?", reponse20, 2) ;


		
		Question[] questions = {question1, question2, question3, question4, question5,
				question6, question7, question8, question9, question10, 
				question11, question12, question13, question14, question15,
				question16, question17, question18, question19, question20} ;
		
		ServerSocket serverSocket = null ;
		
		try {
			serverSocket = new ServerSocket(1234) ;
			System.out.println("En attente de connexion de candidat ...");
			while(true) {
				//Si un client se connecte le serveur lui envoie les questions
				Socket socket = serverSocket.accept() ;
				QcmService qcmService = new QcmService(socket, questions) ;
				qcmService.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
