import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class QcmService extends Thread {

	Socket socket;

	// Donnees attendues
	String nomJoueur;
	Question[] questions;
	Joueur joueur;
	
	public QcmService(Socket socket, Question[] questions) {
		this.socket = socket;
		this.questions = questions ;
	}


	@Override
	public void run() {
		
		
		// Creation des flux de donnees d'entree
		InputStream is;
		InputStreamReader isr;
		ObjectInputStream ois;
		BufferedReader br;

		// Creation des flux de donnees de sortie
		OutputStream out;
		ObjectOutputStream oos;
		PrintWriter pw;

		try {
			
			
			is = socket.getInputStream();

			ois = new ObjectInputStream(is);


			out = socket.getOutputStream();
			oos = new ObjectOutputStream(out) ;
			
			//Confirmation de la connexion du client
			System.out.println(
					"Le client  : " + socket.getRemoteSocketAddress().toString() + 
					" est connect� au serveur");
			
			//Lecture du nom du Joueur
			this.joueur = (Joueur) ois.readObject() ;
			System.out.println("Le client  : " + socket.getRemoteSocketAddress().toString() + 
					" s'est inscrit au concours sous le nom de : " + joueur.nom+ "\n Envoie des questions" );
			
			//Envoie des questions au joueur
			oos.writeObject(questions);
			
			String etatJeu ;
			etatJeu = (String) ois.readObject() ;
			//Si le client envoie "TERMINE" au serveur, le serveur procede au calcul de la note
			if(etatJeu.equals("TERMINE")) {
				//On recupere les differents choix du joueur
				int [] indexChoixJoueur  = (int []) ois.readObject() ; 
				//On calcul son score
				int score = genererScore(questions, indexChoixJoueur) ;
				//On envoie le score au client
				oos.writeObject(score);
				
				System.out.println(joueur.nom + " a termin� le concours avec un score de :" + score + " /20");
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if(joueur != null) {
				System.out.println("Le joueur " +joueur.nom + " a termin� le jeu");

			}
		}

	}
	
		public int genererScore(Question[] questions, int [] indexChoixJoueur) {
		/*
		 * ON verifie chaque la reponse de chaque question avec les choix choix du
		 * joueur
		 */
		int score = 0 ;
		for (int i = 0; i < indexChoixJoueur.length; i++) {
			// Bonne reponse
			Question question = questions[i] ;
			if (question.indexReponseCorrect == indexChoixJoueur[i]) {
				score = score + 1;
				// Le joueur a selectionn� la reponse je ne connais pas
			} else if (indexChoixJoueur[i] == questions[i].reponses.length - 1) {
				score = score + 0;
				// Dans les autres cas la reponse n'est pas correcte
			} else {
				score = score - 1;
			}
		}

		return score;
	}
	


}
