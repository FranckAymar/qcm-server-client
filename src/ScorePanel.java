import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ScorePanel extends JPanel {

	public JLabel remerciementLabel;
	public JLabel remerciementLabel2;
	public JTextField noteFieldScore;
	public JButton recommencerBtn;
	public JLabel totalPoint;

	public ScorePanel() {
		// TODO Auto-generated constructor stub

		this.setLayout(new GridLayout(3, 0));
		
		JPanel hautScorePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel centreScorePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel basScorePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		remerciementLabel = new JLabel();
		remerciementLabel2 = new JLabel("Vous avez obtenu :");
		noteFieldScore = new JTextField();
		noteFieldScore.setPreferredSize(new Dimension(100, 30));
		noteFieldScore.setFont(Police.appliquerCourrierNewBold());
		totalPoint = new JLabel();
		recommencerBtn = new JButton("Terminer");
		recommencerBtn.setFont(Police.appliquerCourrierNew());
		
		// Assignation des polices et bordure aux composants
		remerciementLabel.setFont(Police.appliquerCourrierNew());
		remerciementLabel2.setFont(Police.appliquerCourrierNew());
		
		// Assignation des composants a scorePanel
		hautScorePanel.add(remerciementLabel);
		centreScorePanel.add(remerciementLabel2);
		centreScorePanel.add(noteFieldScore);
		centreScorePanel.add(totalPoint);
		basScorePanel.add(recommencerBtn);

		this.add(hautScorePanel);
		this.add(centreScorePanel);
		this.add(basScorePanel);

	}
}
