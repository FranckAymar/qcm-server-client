import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Fenetre extends JFrame implements Serializable {

	{
		// Appliquer un theme
		try {
			javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Tableau de questions
	Question[] questions = null;
	// Joueur courant
	Joueur joueur = null;

	// Addresse IP du serveur
	String adresseIPServeur = null;

	// Les differents choix du joueur
	int[] indexChoixJoueur;

	// Les differentes vues
	IdentificationPanel identificationPanel = new IdentificationPanel();
	QuestionPanel questionPanel = new QuestionPanel();
	ScorePanel scorePanel = new ScorePanel();

	CardLayout cardLayout = new CardLayout();

	// Container principal de la fenetre
	JPanel container = new JPanel();

	// Socket du client
	Socket socket;

	// Creation des flux de donnees d'entree
	InputStream is;
	InputStreamReader isr;
	BufferedReader br;
	ObjectInputStream ois;

	// Creation des flux de donnees de sortie
	OutputStream out;
	ObjectOutputStream oot;
	PrintWriter pwString;
	PrintWriter pwObject;

	boolean isConnected = false;

	// Constructeur
	public Fenetre() {

		while (isConnected == false) {

			// JOPtion pour recuperer l'adresse IP
			this.adresseIPServeur = JOptionPane.showInputDialog(null,
					"Entrer l'adresse IP du serveur ('localhost' Si vous etes en local)", "QCM Concours Serveur",
					JOptionPane.CLOSED_OPTION);
						
			//Le joueur demande � quitter le jeu
			if(this.adresseIPServeur == null) {
				System.exit(0);
				
			}
			
			// Socket du client
			try {
				connexionAuServeur(this.adresseIPServeur);
				break ;
			} catch (Exception e1) {
				System.out.println("Connexion impossible, v�rifier l'adresse IP");
			}

		}
		
		if(isConnected == true) {
			
			this.setTitle("Concours Esatic");
			this.setSize(1500, 500);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setLocationRelativeTo(null);

			// On initialise le tableau des choix du joueur avec la valeur 4 a toutes les

			questionPanel.btnSuivant.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					QuestionPanel.numeroQuestion++;
					questionPanel.btnPrecedent.setEnabled(true);

					// Le jeu est termin�
					if (QuestionPanel.numeroQuestion > questions.length - 1) {

						try {
							// Envoie des differents choix joueur
							oot.writeObject("TERMINE");
							oot.writeObject(joueur.indexChoix);
							// Recuperation du scre de joeur depuis le serveur
							int score = (int) ois.readObject();
							joueur.score = score;
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						afficherScore();

					}

					// Sinon on genere la question suivante
					else {
						questionPanel.genererQuestionSuivante(questions);
					}
				}
			});

			questionPanel.btnPrecedent.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					QuestionPanel.numeroQuestion--;
					// On desactive le bouton precedent a la premiere question s
					if (QuestionPanel.numeroQuestion == 0) {
						questionPanel.btnPrecedent.setEnabled(false);
					}
					if (QuestionPanel.numeroQuestion < 0) {
						QuestionPanel.numeroQuestion = questions.length - 1;
					}
					questionPanel.genererQuestionSuivante(questions);

				}
			});

			identificationPanel.btnSlideSuivant.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					String nomJoueur = identificationPanel.champNom.getText();
					// Creation du joueur
					joueur = new Joueur(nomJoueur);

					try {
						oot.writeObject(joueur);
						// Recption des questions
						questions = (Question[]) ois.readObject();

						questionPanel.joueur = joueur;

						initialiserLesVariables(questions, joueur);

						questionPanel.genererQuestionSuivante(questions);

						// On demarre le compteur
						questionPanel.compteurPanel.start();

					} catch (Exception e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}

					cardLayout.next(container);

				}
			});

			identificationPanel.champNom.addKeyListener(new KeyAdapter() {

				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					// On met en majuscule le nom entr� par l'utilisateur
					identificationPanel.champNom.setText(identificationPanel.champNom.getText().toUpperCase());
					// Si l'utilisateur entre une lettre le bouton s'active
					if (!identificationPanel.champNom.getText().equals(null)) {
						identificationPanel.btnSlideSuivant.setEnabled(true);
					}

					// Si l'utilisateur n'entre aucune lettre le bouton s

					if (identificationPanel.champNom.getText().equals("")) {
						identificationPanel.btnSlideSuivant.setEnabled(false);

					}
				}

			});

			scorePanel.recommencerBtn.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub

					System.exit(0);

				}
			});

			container.setBorder(new EmptyBorder(20, 20, 20, 20));
			container.setLayout(cardLayout);
			container.add(identificationPanel);
			container.add(questionPanel);
			container.add(scorePanel);

			this.setContentPane(container);
			this.setVisible(true);
			
		}
		
		

	}

	// Initiliase les variables pour recommencer le jeu(score, numero de question,
	// champ de saisie)
	public void initialiserLesVariables(Question[] questions, Joueur joueur) {

		joueur.indexChoix = new int[questions.length];
		for (int i = 0; i < joueur.indexChoix.length; i++) {
			joueur.indexChoix[i] = questions[i].reponses.length - 1;
		}

		QuestionPanel.numeroQuestion = 0;
		questionPanel.btnPrecedent.setEnabled(false);
		identificationPanel.champNom.setText("");
	}

	// Affiche le score du joueur a la fin du jeu
	public void afficherScore() {
		// TODO Auto-generated method stub

		int tempsRestant = questionPanel.compteurPanel.getTempsRestant();

		// Si le jeu se termine lorque le temps expire
		if (tempsRestant == 0) {
			// On met fin au jeu
			try {
				oot.writeObject("TERMINE");
				// On envoie les joueurs du joueur
				oot.writeObject(joueur.indexChoix);
				// Recuperation du scre de joueur depuis le serveur
				int score = (int) ois.readObject();
				joueur.score = score;
			} catch (Exception e) {

			}

		}

		// On procede a l'affichage du score du joueur
		scorePanel.remerciementLabel
				.setText("<html> Merci aurevoir <br> " + this.joueur.nom.toUpperCase() + "  </html>");
		scorePanel.totalPoint.setText("/" + this.questions.length);
		scorePanel.noteFieldScore.setText(String.valueOf(this.joueur.score));

		// On passe au layout suivant
		cardLayout.next(container);
	}


	public void connexionAuServeur(String adresseIPServeur) throws Exception {
		// TODO Auto-generated method stub
		
		if(!adresseIPServeur.equals("") ) {
			
		socket = new Socket(adresseIPServeur, 1234);
		
		//Connexion r�ussie
		out = socket.getOutputStream();
		oot = new ObjectOutputStream(out);

		is = socket.getInputStream();
		ois = new ObjectInputStream(is);

		isConnected = true;
		
		}

	}

}
